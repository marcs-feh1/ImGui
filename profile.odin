package imgui

import "base:runtime"
import "core:mem"
import "core:prof/spall"

ENABLE_PROFILER :: #config(ENABLE_PROFILER, false)

when ENABLE_PROFILER {
	spall_ctx: spall.Context
	spall_buffer: spall.Buffer

	@(private, instrumentation_enter)
	spall_enter :: proc "contextless" (proc_address, call_site_return_address: rawptr, loc: runtime.Source_Code_Location) {
		spall._buffer_begin(&spall_ctx, &spall_buffer, "", "", loc)
	}
	@(private, instrumentation_exit)
	spall_exit :: proc "contextless" (proc_address, call_site_return_address: rawptr, loc: runtime.Source_Code_Location) {
		spall._buffer_end(&spall_ctx, &spall_buffer)
	}
	@(deferred_out=profiler_end)
	profiler_start :: proc() -> (^spall.Context, ^spall.Buffer, []byte){

		profiler_mem := make([]byte, 200 * mem.Megabyte)
		spall_buffer = spall.buffer_create(profiler_mem)
		spall_ctx, _ = spall.context_create("profile.spall")


		return &spall_ctx, &spall_buffer, profiler_mem
	}
	profiler_end :: proc(spall_ctx: ^spall.Context, spall_buffer: ^spall.Buffer, buf: []byte){
		spall.buffer_destroy(spall_ctx, spall_buffer)
		spall.context_destroy(spall_ctx)
		delete(buf)
	}
}
