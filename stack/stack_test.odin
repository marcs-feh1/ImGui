package stack

import "core:slice"
import "core:log"
import ts "core:testing"

@test
small_stack_test :: proc(t: ^ts.T){
	data: [4]int

	stk := create(data[:])
	ts.expect(t, len(stk) == 0)

	ts.expect(t, push(&stk, 4))
	ts.expect(t, push(&stk, 2))
	ts.expect(t, push(&stk, 0))
	ts.expect(t, len(stk) == 3)
	ts.expect(t, slice.equal(items(&stk), []int{4, 2, 0}))

	ts.expect(t, push(&stk, -1))
	ts.expect(t, len(stk) == 4)

	ts.expect(t, !push(&stk, -2))
	ts.expect(t, len(stk) == 4)

	{ v, ok := pop(&stk); ts.expect(t, v == -1 && ok) }
	{ v, ok := pop(&stk); ts.expect(t, v == 0 && ok) }
	{ v, ok := pop(&stk); ts.expect(t, v == 2 && ok) }
	{ v, ok := pop(&stk); ts.expect(t, v == 4 && ok) }
	{ _, ok := pop(&stk); ts.expect(t, !ok) }
}

